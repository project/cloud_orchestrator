<?php

namespace Drupal\cloud_orchestrator\Controller;

/**
 * Interface for DashboardController.
 */
interface DashboardControllerInterface {

  /**
   * Build the dashboard page.
   *
   * @return array
   *   Render array.
   */
  public function getDashboard(): array;

}
