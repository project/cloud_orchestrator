<?php

namespace Drupal\cloud_orchestrator\Plugin\Block;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block displaying a footer message.
 *
 * @Block(
 *   id = "cloud_orchestrator_footer_block",
 *   admin_label = @Translation("Footer Message"),
 *   category = @Translation("Cloud Orchestrator")
 * )
 */
class FooterMessageBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Creates a ResourcesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('Copyright &copy; @year Cloud Orchestrator version: <a href="@link" target="new_window">@version</a>',
        [
          '@year' => date('Y'),
          '@version' => $this->getVersion(),
          '@link' => 'https://www.drupal.org/project/cloud_orchestrator',
        ]),
    ];
  }

  /**
   * Parses cloud_orchestrator.info.yml and loads the version.
   *
   * @return string
   *   The version number.
   */
  private function getVersion() {
    $version = '';
    $module_path = $this->moduleHandler->getModule('cloud_orchestrator')->getPath();
    $info_file_name = $module_path . '/cloud_orchestrator.info.yml';
    if (file_exists($info_file_name) && ($handle = fopen($info_file_name, 'r')) !== FALSE) {
      $contents = fread($handle, filesize($info_file_name));
      if (!empty($contents)) {
        $data = Yaml::decode($contents);
        $version = $data['version'] ?? '';
      }
    }
    return $version;
  }

}
